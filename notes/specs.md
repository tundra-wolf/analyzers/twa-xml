# XML Analyzer - Specs

## XML 1.1

Standards from the World Wide Web Consortium website.

* [XML 1.1](https://www.w3.org/TR/2006/REC-xml11-20060816/)

* [XML 1.1 - Namespaces](https://www.w3.org/TR/2006/REC-xml-names11-20060816/)

* [XML Base](https://www.w3.org/TR/2009/REC-xmlbase-20090128/)

## XSLT

Standards from the World Wide Web Consortium website.

* [XSLT 3.0](https://www.w3.org/TR/xslt-30/)

* [XPath 3.1](https://www.w3.org/TR/xpath-31/)

* [XQuery & XPath Data Model 3.1](https://www.w3.org/TR/xpath-datamodel/)

* [XQuery & XPath Functions and Operators 3.1](https://www.w3.org/TR/xpath-functions-31/)

* [XSL Formatting Objects 2.0 Draft](https://www.w3.org/TR/xslfo20/)

## XML Schema

* [XML Schema 1.1 Part 1: Structures ](https://www.w3.org/TR/xmlschema11-1/)

* [XML Schema 1.1 Part 2: Datatypes](https://www.w3.org/TR/xmlschema11-2/)
